Module reference
================

Serialisation
-------------

.. autofunction:: anamnesis.obj_from_hdf5file

.. autofunction:: anamnesis.obj_from_hdf5group

.. autoclass:: anamnesis.AbstractAnam
   :members:

.. autoclass:: anamnesis.AnamCollection
   :members:

.. autoclass:: anamnesis.Store
   :members:

Class Registration
------------------
.. autofunction:: anamnesis.register_class

.. autofunction:: anamnesis.find_class
