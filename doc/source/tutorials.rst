Tutorials
=========

Serialisation
-------------

.. toctree::
   :maxdepth: 1

   tutorials/tutorial1
   tutorials/tutorial2
   tutorials/tutorial3
   tutorials/tutorial4

MPI
---

.. toctree::
   :maxdepth: 1

   mpitutorials/tutorial1
   mpitutorials/tutorial2
   mpitutorials/tutorial3
